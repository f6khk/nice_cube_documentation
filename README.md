# Nice_cube_documentation
Ce projet rassemble de la documentation partagée par le radioclub d'Antibes F6KHK, avec l'AMSAT, l'Université de Nice et le LEAT autour du projet NiceCube.

## Présentations du projet 
- [Projet initial fin 2019](https://gitlab.com/f6khk/nice_cube_documentation/-/blob/master/Pr%C3%A9sentation_Olivier_26-11-2019.pdf)
- [Présentation aux Journées Spatiales Francophones 2020 de l'AMSAT](https://gitlab.com/f6khk/nice_cube_documentation/-/blob/master/Presentation_NICEcube_rencontre_AMSAT_2020_V3.1.pdf)


## Projet d'emetteur récepteur Opensource
L'université étudie diverses solutions dont celle-ci :

https://tinysdr.org/

Le code source est disponible via la plateforme de partage github https://github.com/uw-x/tinysdr

Tel quel le projet utilise 2 bandes 2.4GHz ou 900MHz.
Il utilise également 2 chips radio : soit une double radio IQ (2.4 et 900 MHz dans leur utilisation), soit un modem SX1272 (uniquement pour le 900MHz)

Pour chaque chaine RF on dispose d'une tête d'entrée incluant préampli, pilote RF et commutateur.

Le chip double radio IQ est le suivant :
https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-42415-WIRELESS-AT86RF215_Datasheet.pdf

Entrée/Sortie 2.4 :
https://eu.mouser.com/datasheet/2/611/johanson-integrated-passives-1666840.pdf
balun spécifique 2.4 GHz

Entrée/Sortie 900 :
https://www.johansontechnology.com/datasheets/0896BM15E0025/0896BM15E0025.pdf
balun spécifique 868/915/928MHz

Chip Lora (9.9mA en RX)
https://www.semtech.com/products/wireless-rf/lora-core/sx1276

pour mémoire (4.6mA en RX):
https://www.semtech.com/products/wireless-rf/lora-transceivers/sx1262

Commutateur pour choisir entre radio IQ ou sx1276 : 
https://www.analog.com/en/products/adg904.html

Switch-LNA(2dB)-PA(21dBm)
https://www.skyworksinc.com/-/media/SkyWorks/Documents/Products/2201-2300/SKY66112_11_203225M.pdf

Switch-LNA(2dB)-PA(30dBm)
https://www.skyworksinc.com/-/media/SkyWorks/Documents/Products/601-700/SE2435L_202412K.pdf



## Renseignements spécifiques concernant le Projet de F4HDK ditr "NPR"

Pour préparer une maquette du Satellite NiceCube, le radioclub F6KHK a proposé de partir d'une solide base existante constituée par le projet déjà bien abouti NPR70 de Guillaume F4HDK. 
Cela permet aux étudiants/participants de démarrer sur un existant.

Fonctionnant en GFSK dans notre cas, il permet d'envisager une modulation plus performante que la traditionnelle audio FSK (utilisée couramment avec le protocole AX25)

Il constitue une base hardware existante à base de STM32 qui permet aux développeurs de logiciel de gagner du temps.

Il ne présume pas des choix techniques que l'équipe NiceCube effectuera pour le satellite.

Un système NPR sur la bande 70cm est installé sur le site du Mont Agel et relié à la maille hamnet 13cm du département.
Cette installation est disponible pour des essais de logiciel pour NiceCube.


* [Source du projet](https://hackaday.io/project/164092-npr-new-packet-radio) publiée par F4HDK sur le site hackaday.io

* Composants du projet NPR :
    * [Spécification du chip radio SI4463](https://www.silabs.com/wireless/proprietary/ezradiopro-sub-ghz-ics/device.si4463) sur la page du fournissseur.
    * [Spécification du module radio](https://www.nicerf.com/product_153_140.html) intégrant un SI4463 et un amplificateur de 1W.

## Renseignements divers
[Spécification du chip radio sx1262](https://www.semtech.com/products/wireless-rf/lora-transceivers/sx1262) utilisé par le LEAT sur un autre prototype.

## Etude d'un réseau d'antennes type Vivaldi pour emetteur
[Projet de Fabien, F4IVF](https://gitlab.com/f6khk/nice_cube_documentation/-/blob/master/EME_avec_un_réseau_de_dipole_sur_plan_de_masse.pdf)
